// Take a look at the license at the top of the repository in the LICENSE file.

use crate::{ConditionPhenomenon, ConditionQualifier, FormatOptions};

use glib::translate::*;

glib::wrapper! {
    #[doc(alias = "GWeatherConditions")]
    pub struct Conditions(BoxedInline<ffi::GWeatherConditions>);
}

impl Conditions {
    #[inline]
    pub fn new(
        significant: bool,
        phenomenon: ConditionPhenomenon,
        qualifier: ConditionQualifier,
    ) -> Self {
        assert_initialized_main_thread!();
        unsafe {
            Self::unsafe_from(ffi::GWeatherConditions {
                significant: significant.into_glib(),
                phenomenon: phenomenon.into_glib(),
                qualifier: qualifier.into_glib(),
            })
        }
    }

    #[inline]
    pub fn significant(&self) -> bool {
        unsafe { from_glib(self.inner.significant) }
    }

    #[inline]
    pub fn phenomenon(&self) -> ConditionPhenomenon {
        unsafe { from_glib(self.inner.phenomenon) }
    }

    #[inline]
    pub fn qualifier(&self) -> ConditionQualifier {
        unsafe { from_glib(self.inner.qualifier) }
    }

    #[doc(alias = "gweather_conditions_to_string")]
    #[doc(alias = "to_string")]
    pub fn to_str(&mut self) -> glib::GString {
        unsafe {
            from_glib_full(ffi::gweather_conditions_to_string(
                self.to_glib_none_mut().0,
            ))
        }
    }

    #[doc(alias = "gweather_conditions_to_string_full")]
    #[doc(alias = "to_string_full")]
    pub fn to_str_full(&mut self, options: &FormatOptions) -> glib::GString {
        unsafe {
            from_glib_full(ffi::gweather_conditions_to_string_full(
                self.to_glib_none_mut().0,
                options.into_glib(),
            ))
        }
    }
}
