# libgweather-rs

The Rust bindings of libgweather

Website: https://world.pages.gitlab.gnome.org/Rust/libgweather-rs

## Documentation

- libgweather: https://world.pages.gitlab.gnome.org/Rust/libgweather-rs/stable/latest/docs/libgweather/index.html
- gweather-sys: https://world.pages.gitlab.gnome.org/Rust/libgweather-rs/stable/latest/docs/gweather_sys/index.html