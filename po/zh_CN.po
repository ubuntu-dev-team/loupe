# Chinese (China) translation for Loupe.
# Copyright (C) 2023 Loupe's COPYRIGHT HOLDER
# This file is distributed under the same license as the Loupe package.
# Nanling <neithern@outlook.com>, 2023.
# lumingzh <lumingzh@qq.com>, 2023-2024.
#
msgid ""
msgstr ""
"Project-Id-Version: Loupe main\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/loupe/-/issues\n"
"POT-Creation-Date: 2024-02-13 22:38+0000\n"
"PO-Revision-Date: 2024-02-24 19:10+0800\n"
"Last-Translator: lumingzh <lumingzh@qq.com>\n"
"Language-Team: Chinese - China <i18n-zh@googlegroups.com>\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0\n"
"X-Generator: Gtranslator 45.3\n"

#: data/org.gnome.Loupe.desktop.in.in:3
#: data/org.gnome.Loupe.metainfo.xml.in.in:6 src/about.rs:29 src/window.rs:880
msgid "Image Viewer"
msgstr "图像查看器"

#: data/org.gnome.Loupe.desktop.in.in:11
msgid "Picture;Graphics;Loupe;"
msgstr "Picture;Graphics;Loupe;图片;图像;照片;图形;放大镜;查看器;观察器;"

#: data/org.gnome.Loupe.gschema.xml.in:6
msgid "Show the image properties sidebar"
msgstr "显示图像属性侧边栏"

#. developer_name tag deprecated with Appstream 1.0
#: data/org.gnome.Loupe.metainfo.xml.in.in:8 src/about.rs:32
msgid "The GNOME Project"
msgstr "GNOME 项目"

#: data/org.gnome.Loupe.metainfo.xml.in.in:12
msgid "View images"
msgstr "查看图像"

#: data/org.gnome.Loupe.metainfo.xml.in.in:14
msgid "Browse through your images and inspect their metadata with:"
msgstr "浏览您的图像并检查其元数据："

#: data/org.gnome.Loupe.metainfo.xml.in.in:16
msgid "Fast GPU accelerated image rendering"
msgstr "快速的 GPU 加速图像渲染"

#: data/org.gnome.Loupe.metainfo.xml.in.in:17
msgid "Tiled rendering for vector graphics"
msgstr "分块渲染矢量图形"

#: data/org.gnome.Loupe.metainfo.xml.in.in:18
msgid "Extendable and sandboxed image decoding"
msgstr "可扩展和沙盒化的图像解码"

#: data/org.gnome.Loupe.metainfo.xml.in.in:19
msgid "Accessible presentation of the most important metadata."
msgstr "对最重要元数据的可访问呈现。"

#: data/resources/gtk/help-overlay.ui:11
msgctxt "shortcut window"
msgid "General Shortcuts"
msgstr "常规快捷键"

#: data/resources/gtk/help-overlay.ui:14
msgctxt "shortcut window"
msgid "Show Main Menu"
msgstr "显示主菜单"

#: data/resources/gtk/help-overlay.ui:20
msgctxt "shortcut window"
msgid "Copy Image or Metadata to Clipboard"
msgstr "复制图像或元数据到剪贴板"

#: data/resources/gtk/help-overlay.ui:26
msgctxt "shortcut window"
msgid "Move Image to Trash"
msgstr "移至回收站"

#: data/resources/gtk/help-overlay.ui:32
msgctxt "shortcut window"
msgid "Permanently Delete Image"
msgstr "永久删除图像"

#: data/resources/gtk/help-overlay.ui:38
msgctxt "shortcut window"
msgid "Toggle Image Properties"
msgstr "切换图像属性"

#: data/resources/gtk/help-overlay.ui:44
msgctxt "shortcut window"
msgid "Set as Background"
msgstr "设为背景"

#: data/resources/gtk/help-overlay.ui:50
msgctxt "shortcut window"
msgid "Toggle Fullscreen"
msgstr "切换全屏"

#: data/resources/gtk/help-overlay.ui:56
msgctxt "shortcut window"
msgid "Leave Fullscreen"
msgstr "退出全屏"

#: data/resources/gtk/help-overlay.ui:62
msgctxt "shortcut window"
msgid "New Window"
msgstr "新建窗口"

#: data/resources/gtk/help-overlay.ui:68
msgctxt "shortcut window"
msgid "Open Help"
msgstr "打开帮助"

#: data/resources/gtk/help-overlay.ui:74
msgctxt "shortcut window"
msgid "Show Keyboard Shortcuts"
msgstr "显示快捷键"

#: data/resources/gtk/help-overlay.ui:80
msgctxt "shortcut window"
msgid "Close Window"
msgstr "关闭窗口"

#: data/resources/gtk/help-overlay.ui:86
msgctxt "shortcut window"
msgid "Quit"
msgstr "退出"

#: data/resources/gtk/help-overlay.ui:94
msgctxt "shortcut window"
msgid "Navigate Images"
msgstr "导航图像"

#: data/resources/gtk/help-overlay.ui:98 data/resources/gtk/help-overlay.ui:112
#: data/resources/gtk/help-overlay.ui:224
#: data/resources/gtk/help-overlay.ui:238
msgctxt "shortcut window"
msgid "Next Image"
msgstr "下一个图像"

#: data/resources/gtk/help-overlay.ui:105
#: data/resources/gtk/help-overlay.ui:119
#: data/resources/gtk/help-overlay.ui:231
#: data/resources/gtk/help-overlay.ui:245
msgctxt "shortcut window"
msgid "Previous Image"
msgstr "上一个图像"

#: data/resources/gtk/help-overlay.ui:125
msgctxt "shortcut window"
msgid "First Image"
msgstr "第一个图像"

#: data/resources/gtk/help-overlay.ui:131
msgctxt "shortcut window"
msgid "Last Image"
msgstr "最后一个图像"

#: data/resources/gtk/help-overlay.ui:139
msgctxt "shortcut window"
msgid "Rotate Image"
msgstr "旋转图像"

#: data/resources/gtk/help-overlay.ui:142
#: data/resources/gtk/help-overlay.ui:263
msgctxt "shortcut window"
msgid "Rotate Clockwise"
msgstr "顺时针旋转"

#: data/resources/gtk/help-overlay.ui:148
#: data/resources/gtk/help-overlay.ui:269
msgctxt "shortcut window"
msgid "Rotate Counterclockwise"
msgstr "逆时针旋转"

#: data/resources/gtk/help-overlay.ui:156
msgctxt "shortcut window"
msgid "Zoom Image"
msgstr "缩放图像"

#: data/resources/gtk/help-overlay.ui:159
#: data/resources/gtk/help-overlay.ui:251
msgctxt "shortcut window"
msgid "Zoom In"
msgstr "放大"

#: data/resources/gtk/help-overlay.ui:165
#: data/resources/gtk/help-overlay.ui:257
msgctxt "shortcut window"
msgid "Zoom Out"
msgstr "缩小"

#: data/resources/gtk/help-overlay.ui:171
msgctxt "shortcut window"
msgid "Best Fit"
msgstr "最适合"

#: data/resources/gtk/help-overlay.ui:177
msgctxt "shortcut window"
msgid "Original Scale"
msgstr "原始比例"

#: data/resources/gtk/help-overlay.ui:183
msgctxt "shortcut window"
msgid "200% Scale"
msgstr "200% 缩放"

#: data/resources/gtk/help-overlay.ui:191
msgctxt "shortcut window"
msgid "Pan View"
msgstr "平移查看"

#: data/resources/gtk/help-overlay.ui:194
msgctxt "shortcut window"
msgid "Pan Left"
msgstr "向左平移"

#: data/resources/gtk/help-overlay.ui:200
msgctxt "shortcut window"
msgid "Pan Right"
msgstr "向右平移"

#: data/resources/gtk/help-overlay.ui:206
msgctxt "shortcut window"
msgid "Pan Up"
msgstr "向上平移"

#: data/resources/gtk/help-overlay.ui:212
msgctxt "shortcut window"
msgid "Pan Down"
msgstr "向下平移"

#: data/resources/gtk/help-overlay.ui:220
msgctxt "shortcut window"
msgid "Gestures"
msgstr "手势"

#. Translators: Replace "translator-credits" with your names, one name per line
#: src/about.rs:41
msgid "translator-credits"
msgstr ""
"Nanling <neithern@outlook.com>\n"
"lumingzh <lumingzh@qq.com>\n"
"Eni <enigma@petalmail.com>, 2023"

#: src/about.rs:42
msgid "Copyright © 2020–2024 Christopher Davis et al."
msgstr "版权所有 © 2020-2024 Christopher Davis 等人。"

#: src/file_model.rs:128
msgid "Could not list other files in directory."
msgstr "无法列出目录中的其他文件。"

#. Translators: short for "north" in GPS coordinate
#: src/metadata/gps.rs:165
msgid "N"
msgstr "北"

#. Translators: short for "south" in GPS coordinate
#: src/metadata/gps.rs:168
msgid "S"
msgstr "南"

#. Translators: short for "east" in GPS coordinate
#: src/metadata/gps.rs:190
msgid "E"
msgstr "东"

#. Translators: short for "west" in GPS coordinate
#: src/metadata/gps.rs:193
msgid "W"
msgstr "西"

#. Translators: Unit for exposure time in seconds
#: src/metadata/mod.rs:260
msgid "1\\u{2215}{}\\u{202F}s"
msgstr "1\\u{2215}{}\\u{202F}秒"

#. Translators: Unit for focal length in millimeters
#: src/metadata/mod.rs:285
msgid "{}\\u{202F}mm"
msgstr "{}\\u{202F}毫米"

#. Translators: This is the date and time format we use in metadata output etc.
#. The format has to follow <https://docs.gtk.org/glib/method.DateTime.format.html>
#. The default is already translated. Don't change if you are not sure what to
#. use.
#: src/util/mod.rs:37
#, c-format
msgid "%x %X"
msgstr "%x %X"

#: src/util/mod.rs:180 src/window.rs:701
msgid "Failed to restore image from trash"
msgstr "从回收站中恢复图像失败"

#: src/util/mod.rs:201
msgid "Failed to open image"
msgstr "打开图像失败"

#: src/widgets/image_page.ui:6 src/window.ui:16
msgid "_Open With…"
msgstr "打开方式(_O)…"

#: src/widgets/image_page.ui:14 src/window.ui:24
msgid "_Print…"
msgstr "打印(_P)…"

#: src/widgets/image_page.ui:18
msgid "_Copy"
msgstr "复制(_C)"

#: src/widgets/image_page.ui:23 src/window.ui:29
msgid "Rotate"
msgstr "旋转"

#: src/widgets/image_page.ui:34 src/window.ui:40
msgid "_Set as Background…"
msgstr "设为背景(_S)…"

#: src/widgets/image_page.ui:63
msgid "Could not Load Image"
msgstr "无法加载图像"

#: src/widgets/image_page.ui:97 src/window.ui:106
msgid "Rotate Left"
msgstr "向左旋转"

#: src/widgets/image_page.ui:107 src/window.ui:116
msgid "Rotate Right"
msgstr "向右旋转"

#. Translators: This is a toast notification, informing the user that
#. an image has been set as background.
#: src/widgets/image_view.rs:682
msgid "Set as background."
msgstr "已设为背景。"

#: src/widgets/image_view.rs:695
msgid "Could not set background."
msgstr "无法设为背景。"

#: src/widgets/image_view.ui:27
msgid "Previous Image"
msgstr "上一个图像"

#: src/widgets/image_view.ui:38
msgid "Next Image"
msgstr "下一个图像"

#: src/widgets/image_view.ui:63
msgid "Zoom Out"
msgstr "缩小"

#: src/widgets/image_view.ui:74
msgid "Zoom In"
msgstr "放大"

#: src/widgets/image_view.ui:85
msgid "Toggle Fullscreen"
msgstr "切换全屏"

#: src/widgets/print.rs:116
msgid "Top"
msgstr "顶部"

#: src/widgets/print.rs:117
msgid "Center"
msgstr "中心"

#: src/widgets/print.rs:118
msgid "Bottom"
msgstr "底部"

#: src/widgets/print.rs:119
msgid "Left"
msgstr "左侧"

#: src/widgets/print.rs:120
msgid "Right"
msgstr "右侧"

#. Translators: Shorthand for centimeter
#: src/widgets/print.rs:156
msgid "cm"
msgstr "cm"

#. Translators: Shorthand for inch
#: src/widgets/print.rs:158
msgid "in"
msgstr "in"

#. Translators: Shorthand for pixel
#: src/widgets/print.rs:160
msgid "px"
msgstr "px"

#. Translators: {} is a placeholder for the filename
#: src/widgets/print.rs:371
msgid "Print “{}”"
msgstr "打印“{}”"

#: src/widgets/print.rs:782
msgid "_Scale"
msgstr "缩放(_S)"

#: src/widgets/print.rs:785
msgid "_Width"
msgstr "宽度(_W)"

#. Translators: Go back to previous page
#: src/widgets/print.ui:29
msgid "_Back"
msgstr "后退(_B)"

#: src/widgets/print.ui:37
msgid "_Print"
msgstr "打印(_P)"

#: src/widgets/print.ui:56
msgid "Layout"
msgstr "布局"

#: src/widgets/print.ui:59
msgid "_Orientation"
msgstr "方向(_O)"

#: src/widgets/print.ui:72
msgid "Portrait Page Orientation"
msgstr "纵向页面方向"

#: src/widgets/print.ui:80
msgid "Landscape Page Orientation"
msgstr "横向页面方向"

#: src/widgets/print.ui:89
msgid "_Alignment"
msgstr "对齐(_A)"

#: src/widgets/print.ui:108
msgid "Margins"
msgstr "边界"

#: src/widgets/print.ui:111
msgid "Margin Units"
msgstr "边界单位"

#: src/widgets/print.ui:128
msgid "_Horizontal"
msgstr "水平(_H)"

#: src/widgets/print.ui:134
msgid "_Vertical"
msgstr "竖直(_V)"

#: src/widgets/print.ui:142 src/widgets/properties_view.ui:53
msgid "Image Size"
msgstr "图像尺寸"

#: src/widgets/print.ui:145
msgid "Image Size Units"
msgstr "图像尺寸单位"

#: src/widgets/print.ui:163
msgid "_Fill Space"
msgstr "填充空间(_F)"

#: src/widgets/print.ui:174
msgid "H_eight"
msgstr "高度(_E)"

#: src/widgets/properties_view.rs:234
msgid "Unknown"
msgstr "未知"

#. Translators: Addition of image being transparent to format name
#: src/widgets/properties_view.rs:238
msgid ", transparent"
msgstr "，透明"

#. Translators: Addition of image being grayscale to format name
#: src/widgets/properties_view.rs:243
msgid ", grayscale"
msgstr "，灰度"

#. Translators: Addition of bit size to format name
#: src/widgets/properties_view.rs:248
msgid ", {}\\u{202F}bit"
msgstr "，{}\\u{202F}bit"

#: src/widgets/properties_view.rs:299
msgid "Image Coordinates Copied"
msgstr "图像坐标已复制"

#: src/widgets/properties_view.ui:19
msgid "Folder"
msgstr "文件夹"

#: src/widgets/properties_view.ui:26
msgid "Open Containing Folder"
msgstr "打开所属文件夹"

#: src/widgets/properties_view.ui:39
msgid "URI"
msgstr "URI"

#: src/widgets/properties_view.ui:62
msgid "Image Format"
msgstr "图像格式"

#: src/widgets/properties_view.ui:71
msgid "File Size"
msgstr "文件大小"

#: src/widgets/properties_view.ui:84
msgid "File Created"
msgstr "创建时间"

#: src/widgets/properties_view.ui:93
msgid "File Modified"
msgstr "修改时间"

#: src/widgets/properties_view.ui:106
msgid "Location"
msgstr "位置"

#: src/widgets/properties_view.ui:116
msgid "Copy Coordinates"
msgstr "复制坐标"

#: src/widgets/properties_view.ui:127
msgid "Open Location"
msgstr "打开位置"

#: src/widgets/properties_view.ui:140
msgid "Originally Created"
msgstr "原始创建"

#: src/widgets/properties_view.ui:149
msgid "Aperture"
msgstr "光圈"

#: src/widgets/properties_view.ui:158
msgid "Exposure"
msgstr "曝光"

#: src/widgets/properties_view.ui:167
msgid "ISO"
msgstr "ISO"

#: src/widgets/properties_view.ui:176
msgid "Focal Length"
msgstr "焦距"

#: src/widgets/properties_view.ui:185
msgid "Maker, Model"
msgstr "制造商，型号"

#: src/window.rs:568
msgid "Supported image formats"
msgstr "支持的图像格式"

#: src/window.rs:574
msgid "All files"
msgstr "所有文件"

#: src/window.rs:581
msgid "Open Image"
msgstr "打开图像"

#: src/window.rs:667
msgid "Image copied to clipboard"
msgstr "图像已复制到剪贴板上"

#: src/window.rs:688
msgid "Image moved to trash"
msgstr "图片已移至回收站"

#: src/window.rs:689
msgid "Undo"
msgstr "撤销"

#: src/window.rs:715
msgid "Failed to move image to trash"
msgstr "将图像移至回收站失败"

#: src/window.rs:741
msgid "Permanently Delete Image?"
msgstr "永久删除图像？"

#: src/window.rs:743
msgid "After deleting the image “{}” it will be permanently lost."
msgstr "删除图像“{}”后它将永久丢失。"

#: src/window.rs:751
msgid "Cancel"
msgstr "取消"

#: src/window.rs:752
msgid "Delete"
msgstr "删除"

#: src/window.rs:763
msgid "Failed to delete image"
msgstr "删除图像失败"

#: src/window.ui:6
msgid "_New Window"
msgstr "新建窗口(_N)"

#: src/window.ui:10
msgid "_Open…"
msgstr "打开(_P)…"

#: src/window.ui:46
msgid "_Help"
msgstr "帮助(_H)"

#: src/window.ui:50
msgid "_Keyboard Shortcuts"
msgstr "快捷键(_K)"

#: src/window.ui:54
msgid "_About Image Viewer"
msgstr "关于图像查看器(_A)"

#: src/window.ui:85
msgid "Copy to Clipboard"
msgstr "复制到剪贴板"

#: src/window.ui:92
msgid "Move to Trash"
msgstr "移至回收站"

#: src/window.ui:98
msgid "Main Menu"
msgstr "主菜单"

#: src/window.ui:130
msgid "Image Properties"
msgstr "图像属性"

#: src/window.ui:174
msgid "View Images"
msgstr "查看图像"

#: src/window.ui:175
msgid "Drag and drop images here"
msgstr "拖放图片到这里"

#: src/window.ui:179
msgid "_Open Files…"
msgstr "打开文件(_O)…"

#~ msgid "The Loupe Team"
#~ msgstr "Loupe 团队"

#~ msgid "Center image horizontally"
#~ msgstr "图像横向居中"

#~ msgid "Center image vertically"
#~ msgstr "图像纵向居中"

#~ msgid "Image Scale"
#~ msgstr "图像缩放"

#~ msgid "Left Margin"
#~ msgstr "左侧边距"

#~ msgid "Length Unit"
#~ msgstr "长度单位"

#~ msgid "_About Loupe"
#~ msgstr "关于(_A)"

#~ msgid "Loupe"
#~ msgstr "Loupe"

#~ msgid "Failed to read image file information"
#~ msgstr "读取图像文件信息失败"

#~ msgid "Failed to decode image"
#~ msgstr "解码图像失败"

#~ msgid "Unknown image format"
#~ msgstr "未知图像格式"

#~ msgid "Animated GIF"
#~ msgstr "GIF 动画"

#~ msgid "Animated WebP"
#~ msgstr "WebP 动画"

#~ msgid "Animated PNG"
#~ msgstr "PNG 动画"

#~ msgid "Unknown image format: {}"
#~ msgstr "未知图像格式：{}"

#~ msgid "Could not open image"
#~ msgstr "无法打开图像"

#~ msgid "Could not read image"
#~ msgstr "无法读取图像"

#~ msgid "Layout Image"
#~ msgstr "布局图像"

#~ msgid "The following features are not available yet"
#~ msgstr "以下功能还不能使用"

#~ msgid "Applying image color profiles"
#~ msgstr "应用图像颜色配置文件"

#~ msgid "Printing"
#~ msgstr "打印"

#~ msgid "Christopher Davis"
#~ msgstr "Christopher Davis"

#~ msgid "Correctly showing SVGs at every zoom-level"
#~ msgstr "在每个缩放级别正确显示SVG"

#~ msgid "Correct HighDPI support"
#~ msgstr "正确的高DPI支持"

#~ msgid "Print settings to position the image on the page"
#~ msgstr "打印设置，以在页面上定位图像"

#~ msgid "File does not exist"
#~ msgstr "文件不存在"

#~ msgid "Failed to open new background image"
#~ msgstr "打开新背景图片失败"

#~ msgid "“{}” is not a valid image."
#~ msgstr "“{}” 不是一个有效的图像。"
